from car import *
import random
import matplotlib.pyplot as plott
import matplotlib.animation as anim
import time as TIME
cars = []
num_cars = 0
time = 0.0
dt=.1
highwaylength=1000
delay=0
firstRun=1

printGraph=1

if printGraph:
    fig=plott.figure()
    ax1=fig.add_subplot(1,1,1)
    plott.ion()

    plott.show()

while len(cars)>0 or firstRun:     #Doesn't work properly when removing cars if < 1000 or <= 1000 is used
    time += 1
    #The five second delay functionality
    if not firstRun:
       if cars[-1].get_vel()<1 and cars[-1].get_pos()<=1:
            delay=5
    firstRun=0
    if delay<=0:
        if num_cars <= highwaylength:
            try:
		cars.append(car(cars[-1]))
            except IndexError:
		cars.append(car())
            num_cars += 1
    else:
        delay-=1

    cars_to_remove=[]
    for k in range(100):
        if 100.0 * random.random() <= 1.0:
            for m in range(len(cars)):
                if k - cars[m].get_pos() <= 1.0 and k - cars[m].get_pos() > 0.0:
                    #cars[m].slowdown(0.5)
                    for i in range(10):
                        try:
                            cars[m+i].slowdown(0.5)
                        except:
                            pass
                    break

    for n in range(10):
	num_del = 0
        for i in range(len(cars)):
            if cars[i-num_del].get_pos() >= 100.0:    #With just >, there'd be a max of 101 cars on the highway
                del cars[i-num_del]
		num_del += 1
            else:               
                cars[i-num_del].update(dt)

    if len(cars) == 0:
        print "broke"
        break

    if printGraph:
        plott.clf()
	plott.axis([0,100,-1,1])
        plott.plot([b.get_pos() for b in cars],[0]*len(cars),"b.")
        TIME.sleep(.01)

        plott.draw()


print cars
print num_cars
print time
